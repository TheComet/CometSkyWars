package co.thecomet.sw.commands;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.world.generator.VoidGenerator;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.sw.SkyWars;
import co.thecomet.sw.config.SkyWarsConfig;
import co.thecomet.sw.config.SkyWarsMapConfig;
import co.thecomet.sw.game.GameLoop;
import co.thecomet.sw.game.phases.PreGamePhase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MaintenanceCommands {
    private static boolean operationInProgress = false;
    
    public MaintenanceCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "setspawn", Rank.ADMIN, MaintenanceCommands::setSpawn);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "mm", Rank.ADMIN, MaintenanceCommands::maintenance);
    }
    
    public static void setSpawn(Player player, String[] args) {
        if (player.getWorld() != Bukkit.getWorlds().get(0)) {
            MessageFormatter.sendErrorMessage(player, "You must be in the main world!");
            return;
        }
        
        SkyWarsConfig config = SkyWars.getSWConfig();
        config.spawn = new JsonLocation(player);
        config.save();
        MessageFormatter.sendSuccessMessage(player, "You have set the spawn to your location.");
    }

    /**
     * Command that enables/disables maintenance mode and
     * handles all necessary command registration.
     * *
     * @param sender
     * @param args
     */
    public static void maintenance(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/mm <true|false>");
            return;
        }
        
        Boolean val;
        try {
            val = Boolean.parseBoolean(args[0]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/mm <true|false>");
            return;
        }
        
        if (val.booleanValue()) {
            if (GameLoop.getInstance().isMaintenanceEnabled()) {
                MessageFormatter.sendErrorMessage(sender, "Maintenance mode is already enabled!");
            } else {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase == false) {
                    MessageFormatter.sendErrorMessage(sender, "Maintenance can only be enabled during pregame phase!");
                    return;
                }
                
                GameLoop.getInstance().setMaintenanceEnabled(true);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpw", Rank.ADMIN, MaintenanceCommands::tpw);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "conf", Rank.ADMIN, MaintenanceCommands::conf);
                CommandRegistry.registerPlayerSubCommand("conf", "setmapname", Rank.ADMIN, MaintenanceCommands::setMapName);
                CommandRegistry.registerPlayerSubCommand("conf", "setauthorname", Rank.ADMIN, MaintenanceCommands::setAuthorName);
                CommandRegistry.registerPlayerSubCommand("conf", "setspectatorspawn", Rank.ADMIN, MaintenanceCommands::setSpectatorSpawn);
                CommandRegistry.registerPlayerSubCommand("conf", "setarenacenter", Rank.ADMIN, MaintenanceCommands::setArenaCenter);
                CommandRegistry.registerPlayerSubCommand("conf", "addpedestal", Rank.ADMIN, MaintenanceCommands::addPedestal);
                CommandRegistry.registerPlayerSubCommand("conf", "clearpedestals", Rank.ADMIN, MaintenanceCommands::clearPedestals);
                CommandRegistry.registerPlayerSubCommand("conf", "tpvalidatepedestals", Rank.ADMIN, MaintenanceCommands::tpValidatePedestals);


                for (Player player : Bukkit.getOnlinePlayers()) {
                    NetworkPlayer bp = CoreAPI.getPlayer(player);
                    if (bp == null || bp.getRank().ordinal() < Rank.MOD.ordinal()) {
                        player.kickPlayer("\nMaintenance mode has been enabled.\nWe are sorry for the inconvenience.");
                    } else {
                        player.setGameMode(GameMode.CREATIVE);
                        player.getInventory().clear();
                        player.updateInventory();
                    }
                }
                
                GameLoop.getInstance().teleportAllToLobby();
            }
        } else {
            if (GameLoop.getInstance().isMaintenanceEnabled()) {
                GameLoop.getInstance().setMaintenanceEnabled(false);
                CommandRegistry.unregisterCommand("tpw");
                CommandRegistry.unregisterCommand("conf");
                CommandRegistry.unregisterCommand("map");
                CommandRegistry.unregisterCommand("chests");
            } else {
                MessageFormatter.sendErrorMessage(sender, "Maintenance mode is not enabled!");
            }
        }
    }

    /**
     * Command to teleport a player to another world.
     * * 
     * @param sender
     * @param args
     */
    public static void tpw(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/tpw <world>");
            return;
        }
        
        if (Bukkit.getWorld(args[0]) != null) {
            if (Bukkit.getWorlds().get(0).equals(Bukkit.getWorld(args[0]))) {
                sender.teleport(SkyWars.getSWConfig().spawn.getLocation());
                return;
            }
        }

        MapManager<SkyWarsMapConfig> manager = GameLoop.getInstance().getMapManager();
        String world = args[0];
        for (File dir : manager.getAllWorlds().values()) {
            if (dir.getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName())) {
                continue;
            }

            if (dir.getName().equalsIgnoreCase(world)) {
                SkyWarsMapConfig config = JsonConfig.load(new File(dir, "map.json"), SkyWarsMapConfig.class);
                config.save(dir);
            } else {
                continue;
            }
            
            World destination = null;
            for (World w : Bukkit.getWorlds()) {
                if (w.getName().equalsIgnoreCase(world)) {
                    destination = w;
                }
            }
            
            if (destination == null) {
                File destDir = new File(Bukkit.getWorldContainer(), dir.getName());
                try {
                    if (destDir.exists()) {
                        FileUtils.deleteDirectory(destDir);
                    }

                    FileUtils.copyDirectory(dir, destDir);
                } catch (IOException e) {
                    MessageFormatter.sendErrorMessage(sender, "Failed to copy the file from the maps folder.");
                    return;
                }
                
                for (File file : destDir.listFiles()) {
                    if (file.getName().equalsIgnoreCase("uid.dat") || file.getName().equalsIgnoreCase("session.lock")) {
                        file.delete();
                    }
                }
                
                destination = new WorldCreator(dir.getName())
                        .generateStructures(false)
                        .environment(World.Environment.NORMAL)
                        .type(WorldType.NORMAL)
                        .generator(new VoidGenerator())
                        .createWorld();
                destination.setAutoSave(false);
                
                for (Entity entity : destination.getEntities()) {
                    if ((entity instanceof Player) == false) {
                        entity.remove();
                    }
                }
            }
            
            sender.teleport(destination.getSpawnLocation());
            MessageFormatter.sendSuccessMessage(sender, "You have been teleported to &6" + destination.getName());
            return;
        }
        
        MessageFormatter.sendErrorMessage(sender, "That world does not exists!");
    }

    /**
     * Main command for map configuration commands.
     * *
     * @param player
     * @param args
     */
    public static void conf(Player player, String[] args) {
        MessageFormatter.sendUsageMessage(player, "/conf setmapname <name>");
        MessageFormatter.sendUsageMessage(player, "/conf setauthorname <name>");
        MessageFormatter.sendUsageMessage(player, "/conf setspectatorspawn");
        MessageFormatter.sendUsageMessage(player, "/conf setarenacenter");
        MessageFormatter.sendUsageMessage(player, "/conf addpedestal");
        MessageFormatter.sendUsageMessage(player, "/conf clearpedestals");
        MessageFormatter.sendUsageMessage(player, "/conf tpvalidatepedestals");
    }
    
    public static void setMapName(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/conf setmapname <name>");
            return;
        }
        
        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }
        
        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        map.getConfig().mapName = StringUtils.join(args, " ");
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "You have set the map name to &6" + map.getMapName());
    }
    
    public static void setAuthorName(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/conf setauthorname <name>");
            return;
        }

        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().mapAuthor = StringUtils.join(args, " ");
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "You have set the map author to &6" + map.getMapAuthor());
    }
    
    public static void addPedestal(Player player, String[] args) {
        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().pedestals.add(new JsonLocation(player));
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "Pedestal set at your position!");
    }

    public static void setSpectatorSpawn(Player player, String[] args) {
        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().spectatorSpawn = new JsonLocation(player);
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "Spectator spawn set to your position!");
    }

    public static void setArenaCenter(Player player, String[] args) {
        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().arenaCenter = new JsonLocation(player);
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "Arena center set to your position!");
    }
    
    public static void clearPedestals(Player player, String[] args) {
        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        map.getConfig().pedestals = new ArrayList<>();
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "Pedestals have been cleared!");
    }
    
    public static void tpValidatePedestals(Player player, String[] args) {
        LoadedMap<SkyWarsMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        if (map.getConfig().pedestals.size() == 0) {
            MessageFormatter.sendErrorMessage(player, "This map has no pedestals configured!");
            return;
        }

        
        for (int i = 0; i < map.getConfig().pedestals.size(); i++) {
            JsonLocation location = map.getConfig().pedestals.get(i);
            Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> player.teleport(location.getLocation()), 20 * (1 + i));
        }
        
        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> MessageFormatter.sendSuccessMessage(player, "You have tp validated all pedestals!"), 20 * map.getConfig().pedestals.size());
    }
}
