package co.thecomet.sw;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CorePlugin;
import co.thecomet.core.moderation.commands.TeleportCommands;
import co.thecomet.sw.config.SkyWarsConfig;
import co.thecomet.sw.game.GameLoop;
import co.thecomet.sw.game.phases.InProgressPhase;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.List;

public class SkyWars extends CorePlugin {
    private static SkyWarsConfig config;
    private static ProtocolManager protocolManager;
    
    @Override
    public void enable() {
        config = initConfig();
        Bukkit.getWorlds().get(0).setAutoSave(false);
        Bukkit.getScheduler().runTaskTimer(this, new GameLoop(this), 20, 20);
        new TeleportCommands(true, Rank.MOD, true, Rank.ADMIN, false, null, false, null, true, Rank.ADMIN);
        
        protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Override
    public void disable() {
        //
    }

    @Override
    public String getType() {
        return "SW";
    }

    @Override
    public String getStatus() {
        if (GameLoop.getInstance() != null && GameLoop.getInstance().getPhase() instanceof InProgressPhase) {
            return "PLAYING";
        }
        
        return super.getStatus();
    }

    @Override
    public List<Class<?>> getDBClasses() {
        return null;
    }
    
    public SkyWarsConfig initConfig() {
        File file = new File(getDataFolder(), "config.json");
        SkyWarsConfig config = JsonConfig.load(file, SkyWarsConfig.class);
        config.save(file);
        return config;
    }
    
    public static SkyWarsConfig getSWConfig() {
        return config;
    }

    public static ProtocolManager getProtocolManager() {
        return protocolManager;
    }
}
