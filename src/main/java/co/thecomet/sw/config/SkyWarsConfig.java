package co.thecomet.sw.config;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.config.JsonLocation;
import org.bukkit.Bukkit;

import java.io.File;

public class SkyWarsConfig extends JsonConfig {
    public JsonLocation spawn = new JsonLocation(Bukkit.getWorlds().get(0).getSpawnLocation());
    public boolean beta = false;

    public void save() {
        File file = new File(CoreAPI.getPlugin().getDataFolder(), "config.json");
        this.save(file);
    }
}
