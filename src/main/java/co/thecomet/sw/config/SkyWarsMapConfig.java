package co.thecomet.sw.config;

import co.thecomet.core.config.JsonLocation;
import co.thecomet.minigameapi.config.BaseConfig;

import java.util.ArrayList;
import java.util.List;

public class SkyWarsMapConfig extends BaseConfig {
    public List<JsonLocation> pedestals = new ArrayList<>();
    public JsonLocation spectatorSpawn;
    public JsonLocation arenaCenter;
}
