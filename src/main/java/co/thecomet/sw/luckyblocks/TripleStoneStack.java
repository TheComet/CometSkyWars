package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Triple Stone Stack", type = LuckyType.NORMAL, dropChance = 0.025)
public class TripleStoneStack extends LuckyBlock {
    private static ItemStack stone = ItemBuilder.build(Material.STONE).amount(64).build();
    
    @Override
    public void onBreak(Block block, Player player) {
        for (int i = 0; i < 3; i++) {
            block.getWorld().dropItem(block.getLocation().add(0.0, 1.0, 0.0), stone);
        }
    }
}
