package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Blaze Squad", type = LuckyType.NORMAL, dropChance = 0.025)
public class BlazeSquad extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        for (int i = 0; i < 5; i++) {
            block.getWorld().spawnEntity(block.getLocation().add(0.5, 0.5, 0.5), EntityType.BLAZE);
        }
    }
}
