package co.thecomet.sw.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.VectorUtils;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

@LuckyInfo(name = "Cluster Grenegg", type = LuckyType.NORMAL, dropChance = 0.025, uniqueRegister = true)
public class ClusterGrenegg extends LuckyBlock implements Listener {
    private static boolean registered = false;
    private ItemStack eggs = ItemBuilder.build(Material.EGG).amount(6).name("Cluster Grenegg").build();
    
    @Override
    public void onBreak(Block block, Player player) {
        block.getWorld().dropItem(block.getLocation().add(0.0, 1.0, 0.0), eggs);
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            if (event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName()) {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("Cluster Grenegg")) {
                    event.setCancelled(true);

                    Entity entity = event.getPlayer().launchProjectile(Egg.class);
                    entity.setMetadata("grenegg", new FixedMetadataValue(CoreAPI.getPlugin(), true));

                    int amount = event.getPlayer().getItemInHand().getAmount();
                    event.getPlayer().getItemInHand().setAmount(amount - 1);
                    
                    if (amount == 1) {
                        event.getPlayer().getInventory().remove(event.getPlayer().getItemInHand());
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Egg == false) {
            return;
        }
        
        if (event.getEntity().hasMetadata("grenegg")) {
            Location start = event.getEntity().getLocation();
            for (int i = 0; i < 5; i++) {
                int x = GeneralUtils.getBetween(1, 3);
                int z = GeneralUtils.getBetween(1, 3);
                
                Location location = event.getEntity().getLocation().add(GeneralUtils.RANDOM.nextBoolean() ? x : -x, 0.0, GeneralUtils.RANDOM.nextBoolean() ? z : -z);
                Entity entity = event.getEntity().getWorld().spawnEntity(start, EntityType.EGG);
                entity.setVelocity(VectorUtils.calculateVelocity(start.toVector(), location.toVector(), GeneralUtils.getBetween(1, 3)));
                entity.setMetadata("sub-grenegg", new FixedMetadataValue(CoreAPI.getPlugin(), true));
            }
        }
        
        if (event.getEntity().hasMetadata("sub-grenegg")) {
            event.getEntity().getWorld().createExplosion(event.getEntity().getLocation(), 2.0f, true);
        }
    }
}
