package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Ghast", type = LuckyType.NORMAL, dropChance = 0.025)
public class Ghast extends LuckyBlock {
    @Override
    public boolean canSpawn(Location loc, Player player) {
        if (loc.getWorld().getHighestBlockAt(loc).getY() == loc.getY()) {
            return true;
        }
        
        return false;
    }

    @Override
    public void onBreak(Block block, Player player) {
        block.getWorld().spawnEntity(block.getLocation().add(0.0, 10.0, 0.0), EntityType.GHAST);
    }
}
