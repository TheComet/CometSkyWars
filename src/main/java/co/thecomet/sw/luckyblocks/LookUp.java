package co.thecomet.sw.luckyblocks;

import co.thecomet.core.chat.MessageService;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Look Up", type = LuckyType.NORMAL, dropChance = 0.025)
public class LookUp extends LuckyBlock {
    private static Material[][][] schematic = new Material[3][4][3];
    
    static {
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 3; x++) {
                for (int z = 0; z < 3; z++) {
                    if (y == 0) {
                        schematic[x][y][z] = Material.MOSSY_COBBLESTONE;
                    } else {
                        if (x == 0 || z == 0 || x == 2 || z == 2) {
                            schematic[x][y][z] = Material.IRON_FENCE;
                        } else {
                            schematic[x][y][z] = Material.AIR;
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public boolean canSpawn(Location loc, Player player) {
        if (player.getWorld().getHighestBlockAt(player.getLocation()).getY() <= player.getLocation().getY()) {
            return true;
        }
        
        return false;
    }

    @Override
    public void onBreak(Block block, Player player) {
        paste(player.getLocation().add(-1, -1, -1));
        Location anvil = player.getLocation().clone();
        anvil.setY(255);
        anvil.getBlock().setType(Material.ANVIL);

        MessageService.sendTitleAnnouncement(player, "Look Up!!!");
    }
    
    public void paste(Location location) {
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 3; x++) {
                for (int z = 0; z < 3; z++) {
                    Location loc = location.clone().add(x, y, z);
                    loc.getBlock().setType(schematic[x][y][z]);
                }
            }
        }
    }
}
