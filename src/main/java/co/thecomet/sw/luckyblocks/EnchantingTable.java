package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Enchanting Table", type = LuckyType.NORMAL, dropChance = 0.025)
public class EnchantingTable extends LuckyBlock {
    private static ItemStack table = ItemBuilder.build(Material.ENCHANTMENT_TABLE).build();
    
    @Override
    public void onBreak(Block block, Player player) {
        block.getWorld().dropItem(block.getLocation().add(0.0, 1.0, 0.0), table);
    }
}
