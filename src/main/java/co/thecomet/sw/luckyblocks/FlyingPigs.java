package co.thecomet.sw.luckyblocks;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.sw.SkyWars;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@LuckyInfo(name = "Flying Pigs", type = LuckyType.NORMAL, dropChance = 0.025)
public class FlyingPigs extends LuckyBlock {
    private static boolean registered = false;
    
    public FlyingPigs() {
        register();
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        for (int i = 0; i < 3; i++) {
            Bat bat = (Bat) block.getWorld().spawnEntity(block.getLocation(), EntityType.BAT);
            bat.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, false, false));
            Pig pig = (Pig) block.getWorld().spawnEntity(block.getLocation(), EntityType.PIG);
            bat.setPassenger(pig);
        }
    }
    
    public static void register() {
        if (registered == false) {
            ProtocolManager protocolManager = SkyWars.getProtocolManager();
            protocolManager.addPacketListener(new PacketAdapter(CoreAPI.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Server.NAMED_SOUND_EFFECT) {
                @Override
                public void onPacketSending(PacketEvent event) {
                    if (event.getPacketType() == PacketType.Play.Server.NAMED_SOUND_EFFECT) {
                        String sound = event.getPacket().getStrings().read(0);
                        if (sound != null && sound.contains("bat")) {
                            event.setCancelled(true);
                        }
                    }
                }
            });
            
            registered = true;
        }
    }
}
