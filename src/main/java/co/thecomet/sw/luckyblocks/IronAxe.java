package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Iron Axe", type = LuckyType.NORMAL, dropChance = 0.025)
public class IronAxe extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack axe = ItemBuilder.build(Material.IRON_AXE).build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), axe);
    }
}
