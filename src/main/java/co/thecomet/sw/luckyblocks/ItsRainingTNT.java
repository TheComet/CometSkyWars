package co.thecomet.sw.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "It's Raining TNT", type = LuckyType.NORMAL, dropChance = 0.025)
public class ItsRainingTNT extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        for (int i = 0; i < 5; i++) {
            int x = GeneralUtils.getBetween(1, 3);
            int z = GeneralUtils.getBetween(1, 3);

            Location location = block.getLocation().clone().add(GeneralUtils.RANDOM.nextBoolean() ? x : -x, 10.0, GeneralUtils.RANDOM.nextBoolean() ? z : -z);
            block.getWorld().spawnEntity(location, EntityType.PRIMED_TNT);
        }
    }
}
