package co.thecomet.sw.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@LuckyInfo(name = "Lucky Hoe", type = LuckyType.NORMAL, dropChance = 0.025, uniqueRegister = true)
public class LuckyHoe extends LuckyBlock implements Listener {
    private static ItemStack hoe = ItemBuilder.build(Material.GOLD_HOE).name(FontColor.rainbow("Lucky Hoe", false, false)).lore("Uses: 15").build();
    private static Cache<UUID, Long> immune = CacheBuilder.newBuilder().expireAfterWrite(50, TimeUnit.MILLISECONDS).build();
    
    @Override
    public void onBreak(Block block, Player player) {
        block.getWorld().dropItem(block.getLocation().add(0.0, 1.0, 0.0), hoe);
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName()) {
                if (ChatColor.stripColor(event.getItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Lucky Hoe")) {
                    immune.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
                    event.getPlayer().getWorld().createExplosion(event.getClickedBlock().getLocation(), 2.0f);

                    int uses = getUses(event.getItem()) - 1;

                    ItemMeta meta = event.getItem().getItemMeta();
                    meta.setLore(new ArrayList<String>() {{
                        add("Uses: " + uses);
                    }});
                    event.getItem().setItemMeta(meta);

                    if (uses == 0) {
                        event.getPlayer().getInventory().remove(event.getItem());
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION || event.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) {
            if (immune.asMap().containsKey(event.getEntity().getUniqueId())) {
                immune.invalidate(event.getEntity().getUniqueId());
                event.setCancelled(true);
            }
        }
    }

    public static int getUses(ItemStack is) {
        if (is.hasItemMeta() && is.getItemMeta().hasLore()) {
            for (int i = 0; i < is.getItemMeta().getLore().size(); i++) {
                String lore = is.getItemMeta().getLore().get(i);
                if (lore.startsWith("Uses: ")) {
                    try {
                        Integer uses = Integer.parseInt(lore.replace("Uses: ", ""));
                        return uses;
                    } catch (NumberFormatException e) {
                        return 0;
                    }
                }
            }
        }

        return 0;
    }
}
