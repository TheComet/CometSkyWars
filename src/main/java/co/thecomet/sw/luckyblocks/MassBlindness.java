package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@LuckyInfo(name = "Mass Blindness", type = LuckyType.NORMAL, dropChance = 0.025)
public class MassBlindness extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (player != p) {
                p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 5, 0));
            }
        }
    }
}
