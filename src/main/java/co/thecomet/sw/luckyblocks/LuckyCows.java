package co.thecomet.sw.luckyblocks;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Cow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Lucky Cows", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckyCows extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        for (int i = 0; i < 5; i++) {
            Cow cow = (Cow) block.getWorld().spawnEntity(block.getLocation(), EntityType.COW);
            cow.setCustomName("Lucky Cow");
            cow.setCustomNameVisible(true);

            Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
                if (cow != null && cow.isDead() == false) {
                    Location location = cow.getLocation();
                    cow.remove();
                    location.getWorld().createExplosion(location, 2.0f);
                }
            }, 20 * 4);
        }
    }
}
