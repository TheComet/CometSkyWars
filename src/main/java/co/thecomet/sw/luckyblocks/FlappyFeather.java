package co.thecomet.sw.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@LuckyInfo(name = "Flappy Feather", type = LuckyType.NORMAL, dropChance = 0.025, uniqueRegister = true)
public class FlappyFeather extends LuckyBlock implements Listener {
    private static ItemStack feather = ItemBuilder.build(Material.FEATHER).name("Flappy Feather").lore("Uses: 8").build();
    private static Cache<UUID, Long> flapping = CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.SECONDS).build();
    private static int id = 1;
    
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack is = feather.clone();
        ItemMeta meta = is.getItemMeta();
        List<String> lore = meta.getLore();
        lore.add("ID: " + id++);
        meta.setLore(lore);
        is.setItemMeta(meta);
        
        block.getWorld().dropItem(block.getLocation().add(0.0, 1.0, 0.0), is);
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName()) {
            if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("Flappy Feather")) {
                event.getPlayer().setVelocity(event.getPlayer().getLocation().getDirection().multiply(2.5));
                flapping.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
                
                int uses = getUses(event.getItem()) - 1;
                
                ItemMeta meta = event.getItem().getItemMeta();
                String id = meta.getLore().get(1);
                meta.setLore(new ArrayList<String>() {{
                    add("Uses: " + uses);
                    add(id);
                }});
                event.getItem().setItemMeta(meta);

                if (uses == 0) {
                    event.getPlayer().getInventory().remove(event.getItem());
                }
            }
        }
    }
    
    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
            if (event.getEntity() instanceof Player) {
                Player player = (Player) event.getEntity();
                if (flapping.asMap().containsKey(player.getUniqueId())) {
                    flapping.invalidate(player.getUniqueId());
                    event.setCancelled(true);
                }
            }
        }
    }

    public static int getUses(ItemStack is) {
        if (is.hasItemMeta() && is.getItemMeta().hasLore()) {
            for (int i = 0; i < is.getItemMeta().getLore().size(); i++) {
                String lore = is.getItemMeta().getLore().get(i);
                if (lore.startsWith("Uses: ")) {
                    try {
                        Integer uses = Integer.parseInt(lore.replace("Uses: ", ""));
                        return uses;
                    } catch (NumberFormatException e) {
                        return 0;
                    }
                }
            }
        }

        return 0;
    }
}
