package co.thecomet.sw.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;

public class Scout extends BaseGameClass {
    public Scout() {
        super("Scout", Material.GOLD_BOOTS, 39);
        
        this.requiredRank = Rank.ULTRA;
        
        this.startingItems.add(ItemBuilder.build(Material.GOLD_SWORD).build());
        this.startingItems.add(ItemBuilder.build(Material.GOLD_LEGGINGS).build());
        this.startingItems.add(ItemBuilder.build(Material.GOLD_BOOTS).build());
        Potion potion = new Potion(PotionType.SPEED);
        potion.getEffects().add(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 3, 0));
        this.startingItems.add(potion.toItemStack(1));
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eGold Sword");
            add(" &7- &eGold Leggings");
            add(" &7- &eGold Boots");
            add(" &7- &ePotion: Swiftness 1, 3 Minutes");
        }});
    }
}
