package co.thecomet.sw.game.classes;

import co.thecomet.core.transaction.CurrencyType;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class Builder extends BaseGameClass {
    public Builder() {
        super("Builder", Material.IRON_PICKAXE, 5);

        this.points = 5000;
        this.feature = new Feature("Builder", "sw-class-builder", points, true, CurrencyType.POINTS);
        
        this.startingItems.add(ItemBuilder.build(Material.COBBLESTONE).amount(32).build());
        this.startingItems.add(ItemBuilder.build(Material.LOG).amount(3).build());
        this.startingItems.add(ItemBuilder.build(Material.IRON_PICKAXE).build());

        this.setMenuItemDescription(new ArrayList<String>() {{
            add("");
            add("&6You will start out with:");
            add(" &7- &e32x Cobblestone");
            add(" &7- &e3x Logs");
            add(" &7- &eIron Pickaxe");
        }});
    }
}
