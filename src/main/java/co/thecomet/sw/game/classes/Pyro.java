package co.thecomet.sw.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class Pyro extends BaseGameClass {
    public Pyro() {
        super("Pyro", Material.TNT, 41);
        
        this.requiredRank = Rank.ULTRA;
        
        this.startingItems.add(ItemBuilder.build(Material.TNT).amount(16).build());
        this.startingItems.add(ItemBuilder.build(Material.REDSTONE_TORCH_ON).amount(16).build());
        this.startingItems.add(ItemBuilder.build(Material.FLINT_AND_STEEL).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &e16x TNT");
            add(" &7- &e16x Redstone Torch");
            add(" &7- &eFlint And Steel");
        }});
    }
}
