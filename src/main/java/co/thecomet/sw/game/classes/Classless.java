package co.thecomet.sw.game.classes;

import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

public class Classless extends BaseGameClass {
    public Classless() {
        super("Classless", Material.SAPLING, 0);
        
        this.startingItems.add(ItemBuilder.build(Material.WOOD_PICKAXE).build());
        this.startingItems.add(ItemBuilder.build(Material.WOOD_AXE).build());
    }
}
