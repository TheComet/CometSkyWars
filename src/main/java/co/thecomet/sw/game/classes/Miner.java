package co.thecomet.sw.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class Miner extends BaseGameClass {
    public Miner() {
        super("Miner", Material.STONE_PICKAXE, 21);
        
        this.requiredRank = Rank.VIP;
        
        this.startingItems.add(ItemBuilder.build(Material.STONE_PICKAXE).build());
        this.startingItems.add(ItemBuilder.build(Material.STONE_SPADE).build());
        this.startingItems.add(ItemBuilder.build(Material.STONE_AXE).build());
        this.startingItems.add(ItemBuilder.build(Material.WOOD).amount(64).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eStone Pickaxe");
            add(" &7- &eStone Spade");
            add(" &7- &eStone Axe");
            add(" &7- &e64x Wood");
        }});
    }
}
