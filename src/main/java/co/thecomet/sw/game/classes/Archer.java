package co.thecomet.sw.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class Archer extends BaseGameClass {
    public Archer() {
        super("Archer", Material.BOW, 19);
        
        this.requiredRank = Rank.VIP;
        
        this.startingItems.add(ItemBuilder.build(Material.BOW).build());
        this.startingItems.add(ItemBuilder.build(Material.ARROW).amount(4).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eBow");
            add(" &7- &e4x Arrows");
        }});
    }
}
