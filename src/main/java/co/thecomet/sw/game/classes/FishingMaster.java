package co.thecomet.sw.game.classes;

import co.thecomet.core.transaction.CurrencyType;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class FishingMaster extends BaseGameClass {
    public FishingMaster() {
        super("Fishing Master", Material.FISHING_ROD, 1);
        
        this.points = 2500;
        this.feature = new Feature("Fishing Master", "sw-class-fishing-master", points, true, CurrencyType.POINTS);
        
        this.startingItems.add(ItemBuilder.build(Material.FISHING_ROD).build());

        this.setMenuItemDescription(new ArrayList<String>() {{
            add("");
            add("&6You will start out with:");
            add(" &7- &eFishing Rod");
        }});
    }
}
