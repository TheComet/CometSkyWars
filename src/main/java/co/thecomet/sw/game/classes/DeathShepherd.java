package co.thecomet.sw.game.classes;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.sw.game.GameLoop;
import co.thecomet.sw.game.player.Participant;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class DeathShepherd extends ActiveBaseGameClass implements Listener {
    private Cache<UUID,Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();
    private List<UUID> projectiles = new ArrayList<>();
    
    public DeathShepherd() {
        super("Death Shepherd", Material.WOOL, 23);

        this.requiredRank = Rank.ULTRA;

        this.startingItems.add(ItemBuilder.build(Material.STICK).name(FontColor.translateString("&eSuper Sheep Shooter 9000")).lore("Uses: 6").build());

        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eSuper Sheep Shooter 9000:");
            add("   &2+ &8Shoots Exploding Sheep");
            add("   &2+ &85 Second Cooldown");
        }});

        setProcessor(new ActiveLoopProcessor() {
            @Override
            public void loop(Player player) {
                World world = Bukkit.getWorld(GameLoop.getInstance().getMapManager().getCurrent().getWorld().getName());
                List<UUID> entities = new ArrayList<UUID>(projectiles);
                for (Sheep sheep : world.getEntitiesByClass(Sheep.class)) {
                    if (entities.contains(sheep.getUniqueId())) {
                        if (sheep.isOnGround()) {
                            explode(sheep);
                        }
                    }
                }
            }
        });
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getItem() != null) {
            Participant participant = GameLoop.getInstance().getParticipant(event.getPlayer().getUniqueId());
            if (participant.getGameClass() == this) {
                if (event.getItem().getType() == Material.STICK && event.getItem().hasItemMeta() && event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(FontColor.translateString("&eSuper Sheep Shooter 9000"))) {
                    if (cooldown.asMap().containsKey(event.getPlayer().getUniqueId()) == false) {
                        Location loc = event.getPlayer().getEyeLocation();
                        Entity entity = event.getPlayer().getWorld().spawnEntity(loc, EntityType.SHEEP);
                        entity.setCustomName("jeb_");
                        entity.setCustomNameVisible(false);
                        entity.setVelocity(event.getPlayer().getLocation().getDirection().multiply(2.5));
                        entity.setMetadata("death sheep", new FixedMetadataValue(CoreAPI.getPlugin(), true));

                        cooldown.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
                        projectiles.add(entity.getUniqueId());
                        int uses = getUses(event.getItem()) - 1;

                        ItemMeta meta = event.getItem().getItemMeta();
                        meta.setLore(new ArrayList<String>() {{
                            add("Uses: " + uses);
                        }});
                        event.getItem().setItemMeta(meta);

                        if (uses == 0) {
                            event.getPlayer().getInventory().remove(event.getItem());
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if (event.getEntity() instanceof Sheep) {
            if (event.getEntity().getCustomName() != null && event.getEntity().hasMetadata("death sheep")) {
                event.setDroppedExp(0);
                event.getDrops().clear();
            }
        }
    }

    public static int getUses(ItemStack is) {
        if (is.hasItemMeta() && is.getItemMeta().hasLore()) {
            for (int i = 0; i < is.getItemMeta().getLore().size(); i++) {
                String lore = is.getItemMeta().getLore().get(i);
                if (lore.startsWith("Uses: ")) {
                    try {
                        Integer uses = Integer.parseInt(lore.replace("Uses: ", ""));
                        return uses;
                    } catch (NumberFormatException e) {
                        return 0;
                    }
                }
            }
        }

        return 0;
    }

    public void explode(Entity entity) {
        Location location = entity.getLocation();
        projectiles.remove(entity.getUniqueId());
        entity.remove();
        location.getWorld().createExplosion(location, 2.0f, true);
    }
}
