package co.thecomet.sw.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class Tank extends BaseGameClass {
    public Tank() {
        super("Tank", Material.CHAINMAIL_CHESTPLATE, 25);
        
        this.requiredRank = Rank.ULTRA;
        
        this.startingItems.add(ItemBuilder.build(Material.CHAINMAIL_HELMET).build());
        this.startingItems.add(ItemBuilder.build(Material.CHAINMAIL_CHESTPLATE).build());
        this.startingItems.add(ItemBuilder.build(Material.CHAINMAIL_LEGGINGS).build());
        this.startingItems.add(ItemBuilder.build(Material.CHAINMAIL_BOOTS).build());
        this.startingItems.add(ItemBuilder.build(Material.IRON_SWORD).build());
        
        this.startingEffects.add(new PotionEffect(PotionEffectType.SLOW, 20 * 60, 0));
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eFull Chainmail Armor");
            add(" &7- &eIron Sword");
            add(" &7- &eEffect: Slowness 1, 1 Minute");
        }});
    }
}
