package co.thecomet.sw.game.classes;

import co.thecomet.core.transaction.CurrencyType;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class BallTosser extends BaseGameClass {
    public BallTosser() {
        super("Ball Tosser", Material.SNOW_BALL, 3);

        this.points = 2500;
        this.feature = new Feature("Ball Tosser", "sw-class-ball-tosser", points, true, CurrencyType.POINTS);
        
        this.startingItems.add(ItemBuilder.build(Material.SNOW_BALL).amount(16).build());

        this.setMenuItemDescription(new ArrayList<String>() {{
            add("");
            add("&6You will start out with:");
            add(" &7- &e16x Snowballs");
        }});
    }
}
