package co.thecomet.sw.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class Warrior extends BaseGameClass {
    public Warrior() {
        super("Warrior", Material.WOOD_SWORD, 7);
        
        this.requiredRank = Rank.VIP;
        
        this.startingItems.add(ItemBuilder.build(Material.LEATHER_CHESTPLATE).build());
        this.startingItems.add(ItemBuilder.build(Material.WOOD_SWORD).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eLeather Chestplate");
            add(" &7- &eWood Sword");
        }});
    }
}
