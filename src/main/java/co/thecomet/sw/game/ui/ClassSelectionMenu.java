package co.thecomet.sw.game.ui;

import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.sw.game.GameLoop;
import co.thecomet.sw.game.classes.*;
import co.thecomet.sw.game.phases.PreGamePhase;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Map;

public class ClassSelectionMenu extends Menu {
    private static Map<Class<? extends GameClass>, GameClass> gameClasses;
    private MenuItem item;
    
    public ClassSelectionMenu() {
        super("Class Selection", 6);
        gameClasses = GameLoop.getInstance().getGameClasses();
        item = new MenuItem("Class Selection", new MaterialData(Material.IRON_AXE)) {
            @Override
            public void onClick(Player player) {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase) {
                    openMenu(player);
                }
            }
        };
        init();
    }
    
    private void init() {
        this.gameClasses.putIfAbsent(FishingMaster.class, new FishingMaster());
        this.gameClasses.putIfAbsent(BallTosser.class, new BallTosser());
        this.gameClasses.putIfAbsent(Builder.class, new Builder());
        this.gameClasses.putIfAbsent(Warrior.class, new Warrior());
        this.gameClasses.putIfAbsent(Archer.class, new Archer());
        this.gameClasses.putIfAbsent(Miner.class, new Miner());
        this.gameClasses.putIfAbsent(DeathShepherd.class, new DeathShepherd());
        this.gameClasses.putIfAbsent(Tank.class, new Tank());
        this.gameClasses.putIfAbsent(Scout.class, new Scout());
        this.gameClasses.putIfAbsent(Pyro.class, new Pyro());
        
        for (GameClass gameClass : new ArrayList<>(gameClasses.values())) {
            addMenuItem(gameClass.getMenuItem(), gameClass.getMenuItem().getSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}
