package co.thecomet.sw.game.ui;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.sw.config.SkyWarsMapConfig;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class MapSelectionMenu extends Menu implements Runnable {
    private int taskId;
    private MenuItem item;
    private MapManager<SkyWarsMapConfig> manager;
    private List<LoadedMap<SkyWarsMapConfig>> configs;
    private Map<UUID, LoadedMap<SkyWarsMapConfig>> votes = new HashMap<>();
    
    public MapSelectionMenu(MapManager<SkyWarsMapConfig> manager) {
        super("Map Selection", 6);
        this.item = new MenuItem("Map Selection", new MaterialData(Material.COMPASS)) {
            @Override
            public void onClick(Player player) {
                if (manager.isUpdating()) {
                    openMenu(player);
                }
            }
        };
        this.manager = manager;
        this.taskId = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), this, 0, 20 * 10).getTaskId();
    }

    @Override
    public void run() {
        init();
    }
    
    private void init() {
        this.configs = manager.getMaps();

        int index = 0;
        for (LoadedMap<SkyWarsMapConfig> map : configs) {
            addMap(map, index);
            index++;
        }
    }
    
    private void addMap(LoadedMap<SkyWarsMapConfig> map, int index) {
        MenuItem item = new MenuItem(map.getMapName(), new MaterialData(Material.MAP)) {
            @Override
            public void onClick(Player player) {
                if (votes.containsKey(player.getUniqueId())) {
                    if (votes.get(player.getUniqueId()) == map) {
                        MessageFormatter.sendErrorMessage(player, "You have already voted for that map!");
                        return;
                    }
                }
                
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (manager.isUpdating() == false) {
                        closeMenu(player);
                    }
                    
                    if (p == player) {
                        MessageFormatter.sendSuccessMessage(p, "You have " + (votes.containsKey(player.getUniqueId()) ? "changed your vote to &6" : "voted for &6") + map.getMapName());
                    } else {
                        MessageFormatter.sendInfoMessage(p, "&6" + player.getName() + " has " + (votes.containsKey(player.getUniqueId()) ? "changed their vote to &6" : "voted for &6") + map.getMapName());
                    }

                    votes.put(player.getUniqueId(), map);
                }
            }
        };
        item.setDescriptions(new ArrayList<String>() {{
            if (map.getMapAuthor() != null) {
                add(FontColor.YELLOW + "Author: " + map.getMapAuthor());
            }
        }});
        
        this.addMenuItem(item, index);
    }
    
    public LoadedMap<SkyWarsMapConfig> getWinningMap() {
        Map<LoadedMap<SkyWarsMapConfig>, Integer> mapCounts = new ConcurrentHashMap<>();
        for (LoadedMap<SkyWarsMapConfig> map : votes.values()) {
            mapCounts.put(map, mapCounts.containsValue(map) ? mapCounts.get(map) + 1 : 1);
        }
        
        LoadedMap<SkyWarsMapConfig> map = null;
        for (Map.Entry<LoadedMap<SkyWarsMapConfig>, Integer> entry : mapCounts.entrySet()) {
            if (map == null) {
                map = entry.getKey();
            } else {
                if (entry.getValue() > mapCounts.get(map)) {
                    map = entry.getKey();
                }
            }
        }
        
        if (map == null) {
            map = manager.getMaps().get(GeneralUtils.getBetween(0, manager.getMaps().size()));
        }
        
        return map;
    }
    
    public LoadedMap<SkyWarsMapConfig> getVotedMap(Player player) {
        return hasVoted(player) ? votes.get(player.getUniqueId()) : null;
    }
    
    public boolean hasVoted(Player player) {
        return votes.containsKey(player.getUniqueId());
    }

    public void removeVote(Player player) {
        votes.remove(player.getUniqueId());
    }

    public MenuItem getItem() {
        return item;
    }

    public int getTaskId() {
        return taskId;
    }
}
