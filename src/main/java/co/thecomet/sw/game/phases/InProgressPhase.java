package co.thecomet.sw.game.phases;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.chat.MessageService;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.items.ItemHelper;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.sw.config.SkyWarsMapConfig;
import co.thecomet.sw.game.Phase;
import co.thecomet.sw.game.classes.ActiveBaseGameClass;
import co.thecomet.sw.game.player.Participant;
import com.google.common.collect.Lists;
import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class InProgressPhase extends Phase implements Listener {
    private final int COUNT_START = 60 * 15;
    private final int COUNT_FOUR_REMAINING = 60 * 2;
    private final int COUNT_TWO_REMAINING = 30;
    private final int COUNT_DEATHMATCH_TIME = 60 * 2;
    
    private boolean testing = false;
    private int count = COUNT_START;
    private SkyWarsMapConfig config;
    private List<Participant> participants;
    private Participant winner;
    private boolean deathmatch = false;
    
    public InProgressPhase(boolean testing) {
        this.testing = testing;
        this.config = loop.getMapManager().getCurrent().getConfig();
        this.participants = loop.getParticipants();

        participants.forEach(participant -> participant.setAlive(true));
    }
    
    @Override
    public void tick() {
        if (participants.size() == 1 && (testing == false || (testing && count < COUNT_START - COUNT_DEATHMATCH_TIME))) {
            winner = participants.get(0);
        }
        
        if (winner != null || participants.size() == 0) {
            World world = Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName());
            world.getWorldBorder().setSize(60000000);
            
            setCompleted();
            return;
        }
        
        if (count == COUNT_START) {
            MessageService.sendFullTitleAnnouncement("BEGIN", "May the odds be in your favor!", 20, 60, 20);

            for (Player player : Bukkit.getOnlinePlayers()) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 10, 1));
                
                Participant participant = loop.getParticipant(player.getUniqueId());
                if (participant != null && participant.getGameClass() != null) {
                    if (participant.getGameClass().getStartingItems() != null && participant.getGameClass().getStartingItems().size() > 0) {
                        for (ItemStack is : participant.getGameClass().getStartingItems()) {
                            if (ItemHelper.isArmor(is)) {
                                ItemHelper.equipArmor(player, is);
                            } else {
                                player.getInventory().addItem(is);
                            }
                        }

                        player.updateInventory();
                    }

                    if (participant.getGameClass().getStartingEffects() != null && participant.getGameClass().getStartingEffects().size() > 0) {
                        player.addPotionEffects(participant.getGameClass().getStartingEffects());
                    }
                }
            }
        }

        loop.getGameClasses().values().stream().filter(gameClass -> gameClass instanceof ActiveBaseGameClass).forEach(gameClass -> {
            ActiveBaseGameClass activeBaseGameClass = (ActiveBaseGameClass) gameClass;
            if (activeBaseGameClass.getProcessor() != null) {
                activeBaseGameClass.loop();
            }
        });

        if (participants.size() <= 6 && count > COUNT_FOUR_REMAINING) {
            if (testing == false || (testing && count == (COUNT_START - COUNT_FOUR_REMAINING))) {
                count = COUNT_FOUR_REMAINING;
            }
        }

        if (count == COUNT_FOUR_REMAINING) {
            MessageService.sendFullTitleAnnouncement((COUNT_FOUR_REMAINING / 60) + " Minutes", "Until Deathmatch!");
        }

        if (participants.size() <= 4 && count > COUNT_TWO_REMAINING) {
            if (testing == false || (testing && count == (COUNT_START - COUNT_FOUR_REMAINING - COUNT_TWO_REMAINING))) {
                count = COUNT_TWO_REMAINING;
            }
        }

        if (count == COUNT_TWO_REMAINING) {
            MessageService.sendFullTitleAnnouncement(COUNT_TWO_REMAINING + " Seconds", "Until Deathmatch!");
        }

        if (count == 0) {
            LoadedMap<SkyWarsMapConfig> map = loop.getMapManager().getCurrent();
            World world = Bukkit.getWorld(map.getWorld().getName());
            if (deathmatch == false) {
                Location location = world.getSpawnLocation();
                if (config.arenaCenter != null) {
                    location = config.arenaCenter.getLocation();
                }

                world.getWorldBorder().setCenter(location);
                world.getWorldBorder().setSize(300.0);
                world.getWorldBorder().setSize(0.0, COUNT_DEATHMATCH_TIME);
                world.getWorldBorder().setDamageAmount(0.5);
                world.getWorldBorder().setDamageBuffer(0.0);
                deathmatch = true;
            } else {
                if (Bukkit.getWorld(map.getWorld().getName()).getWorldBorder().getSize() == 1.0) {
                    Location location = map.getConfig().arenaCenter.getLocation();
                    location.add(0, 10, 0);

                    if (location.getBlock().getType() != Material.LAVA) {
                        location.getBlock().setType(Material.LAVA);
                    }
                }
            }

            return;
        }
        
        count--;
        loop.getGameScoreboardManager().update(false);
    }

    @Override
    public Phase next() {
        return winner != null ? new EndGamePhase(winner) : new EndGamePhase();
    }

    @Override
    public void cleanup() {
        winner = null;
    }

    private static Player getKiller(PlayerDeathEvent event) {
        Player p = event.getEntity();

        if (p.isDead()) {
            if (p.getKiller() instanceof Player) {
                return (Player) p.getKiller();
            }
        }

        return null;
    }

    public int getCount() {
        return count;
    }

    public List<Participant> getParticipants() {
        return participants;
    }
    
    @EventHandler
    public void onCraftItem(CraftItemEvent event) {
        if (event.getRecipe().getResult().getType() == Material.BUCKET) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Participant participant = loop.getParticipant(player.getUniqueId());
        participant.setAlive(false);

        Player killer;
        if ((killer = getKiller(event)) != null) {
            NetworkPlayer bp = CoreAPI.getPlayer(killer);
            int points = 1 * (bp.getRank().ordinal() >= Rank.ULTRA.ordinal() ? 3 : bp.getRank().ordinal() >= Rank.VIP.ordinal() ? 2 : 1);
            CoreDataAPI.addPoints(points, killer, false);
            MessageFormatter.sendInfoMessage(killer, "You have received " + points + " points for killing " + player.getName());
        }

        player.teleport(participant.getPedestal().getLocation());

        participants.remove(participant);
        MessageService.sendTitleAnnouncement(event.getEntity(), FontColor.translateString("&cYOU DIED"));
        MessageService.actionAnnouncement(FontColor.translateString("&6" + player.getName()+ " HAS FALLEN, " + participants.size() + " TRIBUTES REMAINING"));

        if (player.isInsideVehicle()) {
            player.leaveVehicle();
        }

        player.setHealth(20);
        player.setFoodLevel(20);
        player.setGameMode(GameMode.SPECTATOR);

        event.setDeathMessage(null);
        Bukkit.getPluginManager().callEvent(new PlayerRespawnEvent(event.getEntity(), Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName()).getSpawnLocation(), false));
        loop.getGameScoreboardManager().update(false);
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Entity entityDamager = event.getDamager();
        Entity entityDamaged = event.getEntity();

        if(entityDamager instanceof Arrow) {
            if(entityDamaged instanceof Player && ((Arrow) entityDamager).getShooter() instanceof Player) {
                Arrow arrow = (Arrow) entityDamager;

                Vector velocity = arrow.getVelocity();

                Player shooter = (Player) arrow.getShooter();
                Player damaged = (Player) entityDamaged;

                if(isAlive(damaged) == false) {
                    damaged.teleport(entityDamaged.getLocation().add(0, 5, 0));

                    Arrow newArrow = shooter.launchProjectile(Arrow.class);
                    newArrow.setShooter(shooter);
                    newArrow.setVelocity(velocity);
                    newArrow.setBounce(false);

                    event.setCancelled(true);
                    arrow.remove();
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        if (loop.getMapManager().getCurrent().getConfig().spectatorSpawn != null) {
            event.setRespawnLocation(loop.getMapManager().getCurrent().getConfig().spectatorSpawn.getLocation());
        }
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Location location;
        if (loop.getMapManager().getCurrent().getConfig().spectatorSpawn != null) {
            location = loop.getMapManager().getCurrent().getConfig().spectatorSpawn.getLocation();
        } else {
            location = Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName()).getSpawnLocation();
        }

        Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
        participant.setAlive(false);

        event.getPlayer().teleport(location);
        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> event.getPlayer().setGameMode(GameMode.SPECTATOR), 1);
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        for (Participant participant : new ArrayList<>(participants)) {
            if (participant.getUuid() == event.getPlayer().getUniqueId()) {
                participants.remove(participant);
            }
        }
    }
    
    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (isAlive(event.getPlayer()) == false) {
            event.setFormat("&8[&7SPEC&8] " + event.getFormat());
            for (Participant part : participants) {
                Player player = Bukkit.getPlayer(part.getUuid());
                event.getRecipients().remove(player);
            }
        }
    }
    
    @EventHandler
    public void onTarget(EntityTargetLivingEntityEvent event) {
        if (event.getTarget() instanceof Player) {
            if (isAlive((Player) event.getTarget()) == false) {
                event.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (isAlive(event.getPlayer()) == false) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
        if (participants.contains(participant)) {
            return;
        }
        
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
        if (participants.contains(participant)) {
            return;
        }
        
        event.setCancelled(true);
    }
    
    public boolean isAlive(Player player) {
        Participant participant = loop.getParticipant(player.getUniqueId());
        for (Participant part : participants) {
            if (participant == part) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public List<String> getPhaseInfo() {
        return new ArrayList<String>() {{
            add("Testing: " + testing);
            add("Count: " + count);
            add("Participants Remaining: " + participants.size());
            add("Deathmatch: " + deathmatch);
            add("Winner: " + (winner == null ? "n/a" : winner.getName()));
        }};
    }
}
